### Commands

- `SetWaterProfile [float waveAmplitude, float waveFrequency, float horizontalDisplacement, float windSpeed]`
  > Sets the Wave Amplitude, Wave Frequency, Horizontal Displacement, Wind Speed respectively. Must be used prior to loading a save since UltimateWater does not allow change post run-time.

- `SetWeather [string weatherType]`
  > Set the weather to one of the valid scene's `weatherType` immediately.

- `GetCurrentWeatherInformation`
  > Prints the current weather's Wave Amplitude, Wave Frequency, Horizontal Displacement, Wind Speed.

### To Do
- Look into creating a new `Weather` and updating the `WeatherManager`'s `weatherConnections` so that it avoids replacing `Weather_BigWaves` and acts as its own.
  - May cause issues when updating the `weatherConnection` or might not even update right due to run-time limitation.
- Look into tweaking `WaterWave` within `UltimateWater` to see if it's possible to make the waves really janky.
  - Caveat: Probably cause performance issues, ruin existing weather, etc.
