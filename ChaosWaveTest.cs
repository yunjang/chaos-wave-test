﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using HarmonyLib;
using UnityEngine;
using UnityEngine.SceneManagement;
using UltimateWater;

public class ChaosWaveTest : Mod
{
  private Harmony harmony;
  private string instanceId = "com.experimental.chaoswave";

  public static float wavesAmplitudeValue = 4f;
  public static float wavesFrequencyValue = 0.4f;
  public static float horizontalDisplacement = 3f;
  public static float windSpeed = 7.206f;
  
  public void Start() {
    harmony = new Harmony(instanceId);
    harmony.PatchAll(Assembly.GetExecutingAssembly());
    Debug.Log("Mod ChaosWaveTest has been loaded!");
  }

  public void OnModUnload() {
    harmony.UnpatchAll(instanceId);
    Debug.Log("Mod ChaosWaveTest has been unloaded!");
  }

  // Commands
  [ConsoleCommand(name: "SetWaterProfile", docs: "Must be used prior to save file loading. Sets the amplitude, frequency, horizontal displacement, and wind speed respectively.")]
  public static string SetWaterProfile(string[] args) {
    wavesAmplitudeValue = float.Parse(args[0]);
    wavesFrequencyValue = float.Parse(args[1]);
    horizontalDisplacement = float.Parse(args[2]);
    windSpeed = float.Parse(args[3]);
    return $@"Values set to the following:
      Waves Amplitude: {wavesAmplitudeValue}
      Waves Frequency: {wavesFrequencyValue}
      Horizontal Displacement: {horizontalDisplacement}
      Wind Speed: {windSpeed}
    ";
  }

  [ConsoleCommand(name: "SetWeather", docs: "Sets weather to the desired weather type immediately.")]
  public static string SetWeather(string[] args) {
    string weatherType = args[0];
    WeatherManager weatherManager = GameObject.Find("_SP_WeatherManager").GetComponent<WeatherManager>();
    weatherManager.SetWeather(weatherType, true);
    return $"Weather set to: {weatherType}";
  }

  [ConsoleCommand(name: "GetCurrentWeatherInformation", docs: "Gets current Weather's ScriptableObject and its WaterProfileData information.")]
  public static string GetCurrentWeatherInformation(string[] args) {
    WeatherManager weatherManager = GameObject.Find("_SP_WeatherManager").GetComponent<WeatherManager>();
    Weather currentWeather = weatherManager.GetCurrentWeather();
    WaterProfileData data = currentWeather.so_weather.waterProfile.Data;
    return $@"
      Current Weather - {currentWeather.name} - WaterProfileData
      Waves Amplitude: {Traverse.Create(data).Field("_WavesAmplitude").GetValue()}
      Waves Frequency: {Traverse.Create(data).Field("_WavesFrequencyScale").GetValue()}
      Horizontal Displacement: {Traverse.Create(data).Field("_HorizontalDisplacementScale").GetValue()}
      Wind Speed: {Traverse.Create(data).Field("_WindSpeed").GetValue()}
    ";
  }

  // Helpers
  private static void PrintWaterProfileData(string prefix, WaterProfileData data) {
    string logOutput = $@"
      ** [{prefix} - Water Profile Data]
      Waves Amplitude: {Traverse.Create(data).Field("_WavesAmplitude").GetValue()}
      Waves Frequency: {Traverse.Create(data).Field("_WavesFrequencyScale").GetValue()}
      Horizontal Displacement: {Traverse.Create(data).Field("_HorizontalDisplacementScale").GetValue()}
      Wind Speed: {Traverse.Create(data).Field("_WindSpeed").GetValue()}
    ";
    Debug.Log(logOutput);
  }

  // Patches
  [HarmonyPatch(typeof(SceneLoader), "LoadScenes")]
  class SceneLoaderLoadScenes_Patch {
    private static void Prefix() {
      Debug.Log("++ SceneLoader | LoadScenes | Prefix");
      // UltimateWater's system does not allow updates to itself during runtime due to high computational cost.
      GameObject weatherManager = GameObject.Find("_SP_WeatherManager");
      WeatherManager weatherManagerScript = weatherManager.GetComponent<WeatherManager>();

      // Grab the Weather_BigWaves scene to tweak to test.
      GameObject bigWavesWeather = GameObject.Find("Weather_BigWaves");
      WaterProfileData bigWavesWaterProfileData = bigWavesWeather.GetComponent<Weather>().so_weather.waterProfile.Data;
      
      // @TODO: May be best to create a new copy of the Weather_BigWaves and update WeatherManager with it.
      //        So that the ChaosWave can exist as its own weather type.

      // @TODO: Fiddle with WaterWave in UltimateWater. Might be able to tweak the wave to be ridiculous (and buggy I assume).
      //        Problem is this may influence all other weather types and may not work since this all has to be done prior to runtime.

      PrintWaterProfileData("Before", bigWavesWaterProfileData);
      Debug.Log("Setting the new properties before main scene runtime...");
      Traverse.Create(bigWavesWaterProfileData).Field("_WavesAmplitude").SetValue(wavesAmplitudeValue);
      Traverse.Create(bigWavesWaterProfileData).Field("_WavesFrequencyScale").SetValue(wavesFrequencyValue);
      Traverse.Create(bigWavesWaterProfileData).Field("_HorizontalDisplacementScale").SetValue(horizontalDisplacement);
      Traverse.Create(bigWavesWaterProfileData).Field("_WindSpeed").SetValue(windSpeed);
      PrintWaterProfileData("After", bigWavesWaterProfileData);
      Debug.Log("Weather_BigWaves has been updated.");
    }
  }
}